#!/bin/env python

from flask import Flask, request
from flask_restful import Resource, Api
import time
import yum
import pwd

yb = yum.YumBase()
app = Flask(__name__)
api = Api(app)

class Transactions(Resource):
   def get(self):
       yh = yb.history.old(complete_transactions_only=True)
       yh_trans = []
       for transaction in yh:
            dict_yh = {}
            dict_yh['id'] =  transaction.tid
            dict_yh['date'] = time.strftime('%m-%d-%Y %H:%M:%S', time.localtime(transaction.beg_timestamp))
            try:
                dict_yh['login user'] = pwd.getpwuid(transaction.loginuid)[0]
            except:
                dict_yh['login user'] = 'Unset'

            dict_yh['altered'] = len(transaction.trans_data)
            yh_trans.append(dict_yh)

       return yh_trans

class Transaction_Id(Resource):
    def get(self, transaction_id):
        transaction = yb.history.old(tids=[transaction_id])[0]
        dict_yh = {}
        dict_yh['tid'] =  transaction.tid
        dict_yh['loginuid'] = transaction.loginuid

        packages = []
        for pkg in transaction.trans_data:
            packages.append({'package': pkg.name+'-'+pkg.version+'-'+pkg.release+'.'+pkg.arch, 'state': pkg.state})

        dict_yh['packages'] = packages

        return dict_yh

class Package(Resource):
    def get(self, package_name):
        tids = yb.history.search(package_name)
        transactions = yb.history.old(tids=tids)
        results = []
        for transaction in transactions:
           result_dict = {}
           for pkg in transaction.trans_data:
               if pkg.name == package_name:
                   result_dict['id'] = transaction.tid
                   result_dict['action'] = pkg.state
                   result_dict['package'] = pkg.name+'-'+pkg.version+'-'+pkg.release+'.'+pkg.arch
                   results.append(result_dict)

        return results


api.add_resource(Transactions, '/transactions')
api.add_resource(Transaction_Id, '/transactions/<transaction_id>')
api.add_resource(Package, '/package/<package_name>')


if __name__ == '__main__':
    app.run(host='0.0.0.0',port='5002')
