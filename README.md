This program will expose the `yum history` commands via a REST API.

Currently, 3 endpoints are created


/transactions 

returns the list of transactions in the yum history db in JSON format. This is 
equivilant to `yum history list`

/transactions/id

returns the transaction ID details in JSON format. Equivilant to `yum history 
list id`

/package/packageName

Returns the transactions details for that package name. Equivilant to `yum 
history package-info packageName`